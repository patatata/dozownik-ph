
/*
 * CFile1.c
 *
 * Created: 2018-10-25 18:40:13
 *  Author: Bogu
 */ 
#include <util/delay.h>
#include "drivers.h"
void initADC(void)
{
	// select vref=avcc
	ADMUX |=(1<<REFS0);
	// ENABLE ADC, PRESCALER 128 
	ADCSRA |= (1<<ADEN) | (1<<ADPS2) |(1<<ADPS1)| (1<<ADPS0); 
}

uint16_t readADC(uint8_t adc_chanel) 
{
	// zerujemy MUX3/2/1/0 czyli wybor chanelu ADC 
	//Example for adc_channel = 1;
	// ADMUX = 0b0100 0000 & 0b1111 0000 = 0b0100 0000 
	// 0b0100 0000  | (0b0000 0001 & 0b0000 1111)
	// 0b0100 0000 | 0b0000 0001 = 0b0100 0001
	ADMUX = ((ADMUX & 0xF0) | (adc_chanel & 0x0F)) ;
	ADMUX |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC)); //wait for 0 on ADSC
	//adc 10bit = 1024, 
	//1024 - 5V (vref)
	// ADC  - x
	// x = (ADC*5) / 1024 
	return ADC; 
}