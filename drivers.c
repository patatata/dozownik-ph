/*
 * drivers.c
 *
 * Created: 2018-02-02 20:31:06
 *  Author: Bogu
 */ 
#include "drivers.h"
// POWINIEN ZAWIERAC CALE CIALO FUNKCJI 
//funkcja b�dzie zawiera�a wszelkie ustawienia port�w ich kierunki i stany na wej�ciu.
void initDIO(void)
{
	DDRD &= ~PH_LOW; //INT0 jako wejscie
	DDRD &= ~PH_HIGH; //INT1 jako wejscie
	DDRC &= ~SWITCH_PORT_MIN;
	DDRC &= ~SWITCH_PORT_MIN;
	DDRB |= (1<<PORTB0); //wyjscie led
	DDRB |= ACID;//wyjscia do silnika
	DDRB |= ALKALINE;//wyjscia do silnika
	PORTB &= ~ACID;
	PORTB &= ~ALKALINE;
	PORTB &= ~(1<<PORTB0);
	PORTC |= SWITCH_PORT_MIN;
	PORTC |= SWITCH_PORT_PLUS;
	//PORTD |= HIGH_VIB;
	//PORTD |= LOW_VIB;
}

// wystarczy aby tam by�o ustawienia dla przerwa� zewn�trznych oraz ustawienia
//dla timera zliczaj�cego czas, niech on b�dzie w trybie normalnym z preskalerem 1024
void initTimers(void)
{
	//EICRA|=(1<<ISC01) | (1<<ISC00); //Przerwanie INT0 -PD2 //przerwanie od stanu niskiego 
	//EICRA|=(1<<ISC11) | (1<<ISC10);//przerwanie INT1 - PD3
	EIMSK |= (1<<INT1) | (1<<INT0);// wlaczenie przerwan zewnetrznych od INT0&INT0
	TCCR0B|=(1<<CS02) | (1<<CS00);// ustawienie prescalera 1024  1kHZ/1024 /255/ = ok 4 razy nam sie przepeloni timer w ciagu sekundy
	TCCR0B &= ~(1<<CS01);
	sei();//wlaczenie przerwan globalnych
}