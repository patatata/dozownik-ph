/*
 * drivers.h
 *
 * Created: 2018-02-02 20:31:48
 *  Author: Bogu
 */ 
//definicje preprocesora to niech znajduj� si� z pliku drivers.h 
//ustawic makro na ph_min i ph_plus
//odczyt z ph metru sygnalu ph_plus | ph_min
//uruchomienie przerwania zewnetrzego i ustawiene stanu wysokiego na pin od silnika (sprawdzic piny ktore obsluguja przerwanie zewnetrzne)
// zasadzie aby przerwanie by�o jak najkr�tsze!
#include <avr/interrupt.h>// wlaczenie globalne przerwan/ bibliotek sei
#ifndef DRIVERS_H_
#define DRIVERS_H_
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#define F_CPU 1000000UL //czestotliwosci uc
#define PH_LOW (1<<PORTD2)//wejscia od ph,int 0
#define PH_HIGH (1<<PORTD3)//wejscia ph, jak wyzej zmienic
//#define LOW_VIB !(PIND & PH_LOW)
//#define HIGH_VIB !(PIND & PH_HIGH)
#define SWITCH_PORT_MIN (1<<PORTC5)
#define SWITCH_PORT_PLUS (1<<PORTC4)
#define KEY_PH_DOWN !(PINC & SWITCH_PORT_MIN) 
#define KEY_PH_UP !(PINC & SWITCH_PORT_PLUS)
#define ACID (1<<PORTB2)//silnik kwas//
#define ALKALINE (1<<PORTB1)//silnik zasada
#define LED (1<<PORTB0)
#define TOGLE_LED (PORTB ^= LED)
#define LED_INT1 (1<<PORTD7)
#define TOGLE_LED_INT1 (PORTD ^= LED_INT1)
#define LED_INT0 (1<<PORTD6)
#define TOGLE_LED_INT0 (PORTD ^= LED_INT0)
#define LED_TIMER (1<<PORTD5)
#define TOGLE_LED_TIMER (PORTD ^= LED_TIMER)

 
void initDIO(void);
void initTimers(void);
void initADC(void);
uint16_t readADC(uint8_t adc_chanel);




#endif /* DRIVERS_H_ */
