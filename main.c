/*
 * ph_start.c
 *
 * Created: 2018-01-31 21:41:21
 * Author : Bogu
 */ 
#include "drivers.h"
#include "HD44780.h"


#define CHANNEL0 0
volatile int flag_int0=0; //ph low
volatile int flag_int1=0;// ph high volatile dla zmiennych uzywanych w przerwaniu 
volatile uint16_t give_alk; // zmienne globalne maja defaulyt zero
#define F_CPU 1000000UL; //czestotliwosci uc

int main(void)
{
	initTimers(); //wywolanie funkcji Timer
	initDIO();// wywolanie funkcji Portow
	initADC();// inicjalizacja ADC
	static uint8_t pot_value;
	LCD_Initalize(); // inicjalizacja
	LCD_Clear(); // wyczyszczenie wyswietlacza
	LCD_WriteText("Hello World");
	LCD_GoTo(0,1);
	LCD_WriteText("PH Level 6.78");
	_delay_ms(4000);
	//return 0;
	
	
	
	LCD_Home();
	
	
	 while (1) 
    {
		pot_value = (readADC(CHANNEL0)*5) / 1024;
		
		if (pot_value > 2)
		{
			PORTB |= LED;
		}
		else
		{
			PORTB &= ~LED;
		}
		
	}

	
		//jezeli napiecie jest wieksze do 2V to wlacz LED, jezeli mniejsze badz rowne to wylacz led. ZADANIE<<<<<<<<
		//przetransportowac pliki lcd z leckji o lcd i wywolac funkcje, tak aby wyswietlilo sie na lcd hello world 
		/*
		if(KEY_PH_DOWN) //obsluga przycisku min
		{
			_delay_ms(100);
			if(KEY_PH_DOWN)
			{
				PORTB |= ALKALINE;
				TOGLE_LED;
				//_delay_ms(100); // problemy z wylaczaniem silnika jesli posiadamy delay tutaj
			}
		}
			else
			{
				PORTB &=~ALKALINE;
				PORTB &= ~(1<<PORTB0);
			}
			
		if(KEY_PH_UP)
			{
				_delay_ms(100);
				if (KEY_PH_UP)
				{
					PORTB |= ACID;
					TOGLE_LED;
					//_delay_ms(200); problemy z wylaczaniem silnika
				}
			}
			else
			{
				PORTB &= ~ACID;
				PORTB &= ~(1<<PORTB0);
			}
	
	}
	*/
}
		
//ISR defualt nazwa dla przerwan//specjalna funkcja dla avr przerwania tylko argument sie zmienia

ISR(INT0_vect)
{
	if(flag_int0==0)
	{
		flag_int0=1;
		PORTB |= ALKALINE;
		TIMSK0 |= (1<<TOIE0);
		EIMSK &= ~(1<<INT1); //wylaczenie przerwania zewnetrznego doczytac 
		EIMSK &= ~(1<<INT0);// wylaczenie przerwania int 0
		EIFR &= ~(1 << INTF0);
		LED_TIMER;//LED_INT0;//dodac diode jeden kolor
		TOGLE_LED_TIMER;
		
	}
	else
	{
		PORTD &= ~LED_TIMER;
	}
	
}
ISR(INT1_vect)
{
	if(flag_int1==0)
	{
		flag_int1=1;
		PORTB |= ACID;
		TIMSK0 |= (1<<TOIE0);
		EIMSK &= ~(1<<INT1);// wylaczenie przerwania zewnetrznego
		EIMSK &= ~(1<<INT0);// wylaczenie przerwania int 0
		EIFR &= ~(1 << INTF1);	
		LED_TIMER;
		TOGLE_LED_TIMER;
	}
		else
		{
		PORTD &= ~LED_TIMER;
		}
}
ISR(TIMER0_OVF_vect)
{
	if(flag_int0==1)
	{
		give_alk++;//2400 10 min 
		if(give_alk==40u)// liczby tylko dadatnie u jak dostanie to wie ze to tylko liczba dodatnia skraca obliczenia 
		{
			PORTB &= ~ALKALINE;//wylaczamy podawanie kwasu
			LED_INT0;//wylaczenie diody od przerwania 
			_delay_ms(500);
			PORTD |= ~LED_INT0;
		}
	
	}
		if(give_alk==2400u);//czas oczekiwania 
		{
			TIMSK0 &= ~(1<<TOIE0);//wylaczenie timera0
			EIMSK |= (1<<INT1);//wlaczenie przerwan zewnetrznych
			EIMSK |= (1<<INT0);//wlacznie int 0
			LED_INT0;
			TOGLE_LED_INT0;//ustawic diode jak mamy przerwanie rozne kolory
			give_alk = 0;
			flag_int0=0;//Alcaline zerowanie licznika
			flag_int1=0;//acid
		}
	
		
		
		if (flag_int1==1)
		{
			give_alk++;//2400 10 minx	
			if (give_alk==40u)// liczby tylko dadatnie u jak dostanie to wie ze to tylko liczba dodatnia skraca obliczenia
			{
				PORTB &= ~ACID;//wylaczamy podawanie kwasu
				LED_INT1;
				_delay_ms(500);
				PORTD |= ~LED_INT1;
				
			}
			
		}
			
			if(give_alk==2400u);//czas oczekiwania
			{
				TIMSK0 &= ~(1<<TOIE0);//wylaczenie timera0
				
				EIMSK |= (1<<INT1);//wlaczenie przerwan zewnetrznych
				flag_int1=0;//acid 
				LED_INT1;
				TOGLE_LED_INT1;
				flag_int0=0;//alcaline
				give_alk = 0;
			}

		//wlaczyc po 10 min przerwania zewnetrzne give alk /i wylaczyc przerwania od timera zero i wyzerowac Give alk/ 
		//obsluga przerwania int1 co w przypsdku gdy 
		//tez reakcja w timerze
}



